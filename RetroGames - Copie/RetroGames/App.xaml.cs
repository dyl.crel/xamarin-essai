using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using System.IO;
using RetroGames.Views;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace RetroGames
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();
            string path = Path.Combine(
                Environment.GetFolderPath(
                    Environment.SpecialFolder.LocalApplicationData),
                "retro_game.db3");
            SQLiteConnection db = new SQLiteConnection(path);
            db.CreateTable<DAL.Models.Console>();
            db.CreateTable<DAL.Models.User>();
            db.CreateTable<DAL.Models.Eval>();
            db.CreateTable<DAL.Models.Game>();
            Properties.Add("connection", db);

            MainPage = new ConsolesView();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
