﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetroGames.DAL.Models
{
    public class Eval
    {
        public int UserID { get; set; }
        public int GameID { get; set; }
        public double Evaluation { get; set; }
    }
}
