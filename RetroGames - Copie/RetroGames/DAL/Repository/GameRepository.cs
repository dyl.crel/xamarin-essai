﻿using RetroGames.DAL.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetroGames.DAL.Repository
{
    public class GameRepository
    {
        private readonly SQLiteConnection db;
        public GameRepository()
        {
            db = (SQLiteConnection)App.Current.Properties["connection"];
        }
        public Game Get(int ID)
        {
            return db.Get<Game>(ID);
        }

        public int Insert(Game g)
        {
            return db.Insert(g);
        }

        public void Delete(int ID)
        {
            db.Delete<Game>(ID);
        }

        public IEnumerable<Game> GetAll()
        {
            return db.Query<Game>("SELECT * FROM [Game]");
        }

        public void Update(Game g)
        {
            db.Update(g);
        }
    }
}
