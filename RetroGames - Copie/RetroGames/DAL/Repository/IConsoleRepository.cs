﻿using System.Collections.Generic;
using RetroGames.DAL.Models;

namespace RetroGames.DAL.Repository
{
    interface IConsoleRepository
    {
        void Delete(int ID);
        Console Get(int ID);
        IEnumerable<Console> GetAll();
        int Insert(Console c);
        void Update(Console c);
    }
}