﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RetroGames.MVVM
{
    static class MapperExtension
    {
        public static TTo Map<TFrom, TTo>(this object from, params object[] parameters)
        {
            IEnumerable<PropertyInfo> P1 = typeof(TFrom).GetProperties();
            IEnumerable<PropertyInfo> P2 = typeof(TTo).GetProperties();

            ConstructorInfo ctor = typeof(TTo).GetConstructors().FirstOrDefault();

            TTo result = (TTo)ctor.Invoke(parameters);
            foreach (PropertyInfo PI1 in P1.Where(p => p.CanRead))
            {
                foreach (PropertyInfo PI2 in P2.Where(p => p.CanWrite))
                {
                    if (PI1.Name == PI2.Name)
                    {
                        PI2.SetValue(result, PI1.GetValue(from));
                    }
                }
            }
            return result;
        }
    }
}