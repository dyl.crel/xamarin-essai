﻿using RetroGames.MVVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetroGames.ViewModels
{
    class ConsoleViewModel : ViewModelBase
    {
        private int _ID;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; RaisePropertyChanged(); }
        }
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; RaisePropertyChanged(); }
        }


    }
}
