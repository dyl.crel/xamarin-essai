﻿using RetroGames.DAL.Models;
using RetroGames.DAL.Repository;
using RetroGames.MVVM;
using RetroGames.Views;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace RetroGames.ViewModels
{
    class ConsolesViewModel : CollectionViewModelBase<ConsoleViewModel>
    {
        protected override ObservableCollection<ConsoleViewModel> LoadItems()
        {
            return new ObservableCollection<ConsoleViewModel>(ServiceLocator.Instance.GetService<IConsoleRepository>().GetAll().Select(c => c.Map<Console,ConsoleViewModel>()));
        }

        private readonly INavigation _navigation;

        public ICommand AddConsoleCommand { get; }

        //public ConsolesViewModel(INavigation nav)
        //{
        //    _navigation = nav;
        //    AddConsoleCommand = new Command(AddConsole);
        //}
        public ConsolesViewModel()
        {
            AddConsoleCommand = new Command(AddConsole);
        }
        private void AddConsole()
        {
            App.Current.MainPage.Navigation.PushModalAsync(new AddConsoleView());
        }
    }
}
